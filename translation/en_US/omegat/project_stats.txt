04.10.24, 10:15
Projektstatistiken

                     	Segmente	Wörter	Zeichen (ohne Leerzeichen)	Zeichen (mit Leerzeichen)	Dateien	
Gesamt:              	    1012	 15847	                    117866	                   131910	      2	
Verbleibend:         	       1	   194	                      1634	                     1657	      1	
Einmalig:            	     970	 15660	                    116386	                   130313	      2	
Verbleibend einmalig:	       1	   194	                      1634	                     1657	      1	


Statistik einzelner Dateien:

Dateiname    	Segmente Gesamt	Verbleibende Segmente	Einmalige Segmente	Verbleibende einmalige Segmente	Wörter Gesamt	Verbleibende Wörter	Einmalige Wörter	Verbleibende einmalige Wörter	Zeichen Gesamt (ohne Leerzeichen)	Verbleibende Zeichen (ohne Leerzeichen)	Einmalige Zeichen (ohne Leerzeichen)	Verbleibende einmalige Zeichen (ohne Leerzeichen)	Zeichen Gesamt (mit Leerzeichen)	Verbleibende Zeichen (mit Leerzeichen)	Einmalige Zeichen (mit Leerzeichen)	Verbleibende einmalige Zeichen (mit Leerzeichen)	
GBP-Title.tex	             12	                    0	                12	                              0	           47	                  0	              47	                            0	                              575	                                      0	                                 575	                                                0	                             602	                                     0	                                602	                                               0	
GBP-Part2.tex	           1000	                    1	               958	                              1	        15800	                194	           15613	                          194	                           117291	                                   1634	                              115811	                                             1634	                          131308	                                  1657	                             129711	                                            1657	
