% !TEX root = BuildWithGBP.nw.tex
\part{Überblick}
\chapter{Lizenz}

Der Text des Buches \enquote{\texttt{Debian}-Pakete bauen mit \textit{Git-Buildpackage}} von 
Mechtilde und Michael Stehmann steht unter der \index{Lizenz}
\textbf{Creative Commons Namensnennung - Weitergabe unter gleichen Bedingungen
4.0 International Lizenz \index{Creative Commons}
(CC BY-SA 4.0)}\cite{CreativeCommon40-2013}.

Das beschriebene Programmskript steht unter der \textbf{GNU General Public License 
Version 3} \index{GNU General Public License} oder nach Ihrer Wahl einer 
späteren Version\cite{GPLv3}.

Copyright: \textsuperscript{\textcopyright} 2012-2024 Mechtilde Stehmann
(E-Mail: mechtilde@debian.org),
\newline Michael Stehmann (E-Mail: michael@canzeley.de)

\chapter{Zum Titel des Buches}

Was ein \textbf{\texttt{Debian}-Paket} ist, wird in
Kapitel~\ref{chap:DebianPackage} (Seite~\pageref{chap:DebianPackage})
beschrieben. Vorab sei verraten: Ein \texttt{Debian}-Paket ist nicht nur
ein Archiv im \textit{deb}-Format, das Binär-Dateien enthält.

\textit{git-buildpackage} ist ein \texttt{Debian}-Paket, welches nützliche
Werkzeuge enthält, \texttt{Debian}-Pakete aus einem \texttt{Git}-Repositorium
zu bauen. Einige dieser Werkzeuge werden vom Programmskript, das in diesem
Buch beschreiben wird (Kapitel~\ref{chap:FirstSteps},
Seite~\pageref{chap:FirstSteps}), verwandt (s.~Kapitel~\ref{chap:SystemSetup},
Seite~\pageref{chap:SystemSetup}).

Die Namen der Werkzeuge in diesem Paket fangen mit \textit{gbp} an. Ein wichtiges Werkzeug
ist \textit{gbp~buildpackage}.

\chapter{Wer sollte dieses Buch lesen?}

Die Ausführungen in diesem Buch sind besonders für die Nutzer des Skriptes 
interessant. 
Aber auch Menschen, die sich allgemein für das Paketieren \index{Paketieren}
für eine Distribution 
interessieren, werden in diesem Buch Informationen finden können.

Dieses Buch will kein \enquote{Lehrbuch} des \texttt{Debian}-Paketbaus sein. 
Es ist eher ein Erfahrungsbericht, wobei die Erfahrungen \enquote{in Code 
gegossen} worden sind.

Das Buch beschreibt, wie \texttt{Debian}-Pakete auf der Basis eines 
\texttt{Git}-Repo\-si\-toriums mit den Programmen aus dem Paket
\textit{git-buildpackage} \cite{Guenther2006-2017} und anderen 
nützlichen Befehlen erstellt werden. Am Ende sollte der Leser in der Lage 
sein, mit der Hilfe des dargestellten Programmskripts und der Beschreibungen der 
einzelnen Schritte \enquote{veröffentlichungsreife} 
\texttt{Debian}-Pakete zu bauen.

Das Programmskript selbst baut \textbf{keine} \texttt{Debian}-Pakete, 
sondern unterstützt den Nutzer beim Bauen derselben. Es ist nur ein
Assistenzprogramm.

Dieses Buch kann auch dazu dienen, Probleme zu verstehen, die beim 
Paketieren von \texttt{Debian}-Paketen auftreten können.

Paketieren ist im Grunde nicht schwierig. 
Es gibt aber immer wieder neue Herausforderungen. 
Paketieren macht daher Spaß.

\chapter{Wie entsteht dieses Buch?}
\section{Motivation}

Was treibt uns, ein solches Buch zu schreiben?

Dazu muss man folgendes wissen:

Beim Paketieren werden viele Befehle in einer sinnvollen Reihenfolge in 
der Shell ausgeführt. Außerdem müssen viele kleine Dateien gepflegt und 
eingebunden werden. Kleinste Fehler und Ungenauigkeiten führen meist dazu, dass 
das Paket nicht korrekt gebaut werden kann. Auch ist es aufwändig und 
fehlerträchtig, immer wieder die korrekten Optionen einzusetzen.

Um diese Fehlerquellen möglichst klein zu halten, wurden diese Schritte
in einem Shell-Skript zusammengefasst. Im Laufe der Zeit und mit jedem
weiteren Paket wächst dieses Skript und wird auch immer weiter verfeinert.
Daraus ist bisher schon ein umfangreiches Programmskript geworden.

Als Mechtilde anfing, sich mit dem Bauen von \texttt{Debian}-Paketen zu 
beschäftigen, stand die Frage im Raum, wie sich diese vielen Schritte 
dokumentieren und automatisieren lassen. Zur Automatisierung ist dann dieses 
Shell-Skript entstanden. Der Bedarf an Dokumentation konnte von Anfang an nicht 
mit Kommentaren -- weder in den einzelnen Dateien, noch in diesem Programmskript -- 
gedeckt werden.

Deshalb haben wir auch schon früh angefangen, unsere Schritte beim Paketieren 
ausführlich festzuhalten. Ein besonderes Augenmerk haben wir auf
Beschreibungen gelegt, welche getroffene Entscheidungen nachvollziehbar und 
überprüfbar machen. Dies erleichtert notwendige Veränderungen.

Daher haben wir auch soweit wie möglich bei der Angabe von Optionen die 
Langform gewählt. Dies erleichtert die Lesbarkeit.

Die Dokumentation soll also sowohl das eigentliche Paketieren beschreiben, 
als auch das Skript erläutern.

Die \texttt{Debian}-Distribution \index{Distribution} ist das Werk vieler 
Menschen. Sie besteht aus mehreren zehntausenden Paketen. 
Das Bauen der Pakete ist eine wesentliche Aufgabe der Paketbetreuer. 
Viele Paketbetreuer nutzen hierzu eigene Skripte. 
Die Veröffentlichung eines solchen Skriptes ist daher ein Wagnis. 
Wenn unser Skript einigen Paketbetreuern das Leben erleichtert und Neulinge 
an das Paketbauen heranführt, hat sich dieses Wagnis gelohnt.

Das beschriebene Programmskript bezieht sich nicht auf ein bestimmtes 
\texttt{Debian}-Paket. 
Vielmehr sollen damit generell einfache \texttt{Debian}-Pakete gebaut 
werden können.

Es werden die Schritte beschrieben, die wir für das Paketieren der von 
Mechtilde betreuten Pakete benötigen. Das Programmskript erhebt nicht 
den Anspruch, man könne damit aus jedem Quellcode ein \texttt{Debian}-Paket bauen.

An vielen Stellen kann und muss man auch manuell eingreifen. 
Hierzu soll die Beschreibung der Vorgänge beim Paketieren eine Hilfe sein. 

Dass das Programmskript die Möglichkeit manueller Eingriffe voraussetzt, 
macht es erforderlich, dass das Programmskript immer wieder prüft, ob 
die Voraussetzungen, von denen die Verfasser ausgegangen sind, auch 
tatsächlich vorliegen. Diese Notwendigkeit erhöht leider den Umfang und 
die Komplexität des Skriptes und damit auch den Umfang des Buches.

\section{Baustellen}

Das Buch und das Skript sind immer noch \enquote{Baustellen}, weil 
immer wieder neue Erfahrungen einfließen.

Das Buch ist in deutscher, die Oberfläche des Programmskripts in englischer 
Sprache verfasst. 
Lokalisierungen sind willkommen. Als \enquote{Proof-of-Concept} wurde bereits
ein Teil des Buches in die englische Sprache übersetzt.%
\footnote{\url{https://ddp-team.pages.debian.net/dpb/en_US/BuildWithGBP.pdf}}

Die Veröffentlichung \index{Veröffentlichung} des Quelltextes \index{Quelltext} 
erfolgt im vom \texttt{Debian}-Projekt bereitgestellten 
\texttt{Git}-Repositorium\footnote{\url{https://salsa.debian.org/ddp-team/dpb}}. 
Dort ist die \textit{CI/CD} (Kapitel~\ref{sec:gitlab-ci.yml}, 
Seite~\pageref{sec:gitlab-ci.yml}) aktiviert. Daher steht das Buch als
\textit{*.pdf}\footnote{\url{https://ddp-team.pages.debian.net/dpb/BuildWithGBP.pdf}} und 
\textit{*.epub}\footnote{\url{https://ddp-team.pages.debian.net/dpb/BuildWithGBP.epub}}
zur Verfügung. Dort können auch die Programmskripte heruntergeladen 
werden\footnote{\url{https://ddp-team.pages.debian.net/dpb/build-gbp.sh}
\newline
\url{https://ddp-team.pages.debian.net/dpb/build-gbp-maven-plugin.sh}\newline
\url{https://ddp-team.pages.debian.net/dpb/build-gbp-webext-plugin.sh}\newline
\url{https://ddp-team.pages.debian.net/dpb/build-gbp-python-plugin.sh}\newline
\url{https://ddp-team.pages.debian.net/dpb/build-gbp-java-plugin.sh}
}

\section{Werkzeuge}
\enquote{Schöner Leben mit Dokumentation} ist ein geflügeltes Wort 
in unserer \textit{Peergroup}.

Mit welchen \index{Werkzeuge} Werkzeugen lässt sich eine solche 
Dokumentation erstellen?
Stehen diese Werkzeuge auch als \texttt{Debian}-Pakete zur Verfügung?

Einen wichtigen Hinweis dazu hat Mechtilde auf einer Veranstaltung zum 
\texttt{Software Freedom Day 2012}
\footnote{\url{https://sfd.koelnerlinuxtreffen.de/SFD2012/2012Robert_Stanowsky.html}}
in Köln erhalten. Dort hat sie die Möglichkeiten von \texttt{noweb}
\cite{NoWeb2018}\footnote{s.a. \url{https://de.wikipedia.org/wiki/Noweb}} 
\index{noweb} kennengelernt.
Dies bedeutete auch, sich mit \LaTeX{} \index{\LaTeX{}}näher zu beschäftigen.

In dieser Kombination ist es möglich, Code und dessen Beschreibung in 
\emph{einem} Dokument zu pflegen. Donald Knuth bezeichnet dies als 
\index{Literate Programming}
\enquote{Literate Programming}\footnote{\url{http://www.literateprogramming.com/}}

Weiter haben wir uns damit beschäftigt, dass sich mit \LaTeX{} neben 
PDF-Dokumenten \index{PDF-Dokument} auch Dokumente im \texttt{EPUB}-Format 
\index{EPUB-Dokument} erstellen lassen. Diese können auch mit einem E-Book-Reader 
\index{E-Book} gelesen werden. (Kapitel~\ref{sec:CreateBook}, 
Seite~\pageref{sec:CreateBook})

Dieses Buch in eine andere Sprache zu übersetzen, stellt eine besondere 
Herausforderung da. Unsere Tests haben ergeben, dass \texttt{OmegaT}
\footnote{\url{https://packages.debian.org/sid/omegat}}
insoweit ein nützliches und komfortables Werkzeug darstellt. Der
entsprechende Prozess wird in einem separaten Büchlein dokumentiert
\footnote{\url{https://oldmike.pages.debian.net/omegatbooklet/omegat.pdf}}.

Das Literaturverzeichnis wird mit \textit{jabref} erstellt und gepflegt. 
Die so erstellte Datei kann in das \LaTeX{}-Dokument eingebunden werden.

Als Editor wird \textit{geany} \index{Geany} mit dem Plugin 
\textit{geany-plugin-latex} verwendet.

\textit{Git} \index{git} ist genial. 
Das Bauen erfolgt daher mit den Werkzeugen aus dem Paket \index{git-buildpackage} 
\textit{git-buildpackage}\footnote{\url{https://packages.debian.org/sid/git-buildpackage}}
(s. Kapitel~\ref{sec:ScriptDependencies},
Seite~\pageref{sec:ScriptDependencies}).

Die Menschen bei \texttt{Debian} haben viele nützliche Programme geschaffen, 
die das Bauen von \texttt{Debian}-Paketen erleichtern und vereinheitlichen. 
Das dargestellte Programmskript wurde daher \enquote{auf den Schultern 
von Riesen} geschaffen. 

Es dient dazu, die eingesetzten Hilfsprogramme in zweckmäßiger Reihenfolge 
aufzurufen und mit sinnvollen Optionen zu versehen. 
Es soll seinen Nutzern den Weg weisen und ihnen die Arbeit erleichtern.
Um seine Anpassung an die Bedürfnisse seiner Nutzer zu erleichtern, ist 
es ein Shellskript.

\chapter{Konventionen}

Einige Hinweise zum besseren Verständnis des Buches:

\section{System}

Das Buch und besonders das Programmskript wurden auf einer \textit{64-Bit-PC}-
Architektur erstellt. Diese wird bei \texttt{Debian} als \textit{amd64} 
bezeichnet. Eine weitere Bezeichnungen für dieses System ist \textit{x86-64}.

\section{Terminologie}

Ein \textbf{neues Paket} ist ein Paket, welches das Programmskript noch 
nicht kennt. D.h. zu diesem Paket existiert noch keine Konfigurationsdatei.

Eine \textbf{neue Version} ist eine neue Upstream-Version. Dem Bauen einer 
neuen Version folgt der Bau einer neuen Revision.

Eine \textbf{neue Revision} bezeichnet ein neu hochzuladenes \texttt{Debian}-Paket.

\section{Typographie}
Alle Programmnamen sind in \textit{kursiv} gesetzt.
Alle Eigennamen sind in \texttt{nicht-proportio\-naler} Schrift gesetzt.
Hochgestellte Zahlen weisen auf die Fußnoten auf der gleichen Seite hin.
Zitate auf ein Gesamtdokument weisen in eckigen Klammern [~] direkt auf 
das Literaturverzeichnis.

Alle Optionen der Shell-Kommandos werden in der Langform angegeben, soweit 
dies möglich ist. Dies erhöht die Lesbarkeit.

Für die verwendeten Abkürzungen wird auf die Einträge im Glossar
\footnote{\url{https://wiki.debian.org/Glossary}} verwiesen.

\section{Darstellung des Quellcodes}

Die Darstellung des Quellcodes erfolgt in Teilstücken (sogenannten 
Code Chunks). Die Reihenfolge dieser Code-Teile im Buch entspricht 
oft nicht der Reihenfolge in den Skripten. Dass die Reihenfolge in Buch 
und Skript nicht übereinstimmen muss, ist ein Vorzug von \texttt{noweb}.

\chapter{Kurzanleitung}

Dieser Abschnitt \index{Kurzanleitung} soll dem Nutzer helfen, die
benötigten Schritte nachzuvollziehen und deren Beschreibung schneller
im Buch zu finden.

\section{Vorbereitung der Build-Umgebung}

Bis aus dem Buch und den Skripten ein \texttt{Debian}-Paket entsteht, sind
die folgenden Schritte notwendig,

Das Programmskript selbst baut \textbf{keine} \texttt{Debian}-Pakete, 
sondern unterstützt den Nutzer beim Bauen derselben. Es ist nur ein
Assistenzprogramm.

\begin{enumerate}
\item Herunterladen von \textit{salsa.debian.org}
Der Quelltext dieses Buches und die folgenden beiden Skripte finden sich unter
\textit{\url{https://salsa.debian.org/ddp-team/dpb}}.

\item Installieren der Abhängigkeiten, um das Buch und die Build-Skripte
lokal zu generieren. (Kapitel~\ref{subsec:BookDependencies},
Seite~\pageref{subsec:BookDependencies})

\item Generieren des PDF mit \textit{./create-book.sh}
(Kapitel~\ref{sec:CreateBook}, Seite~\pageref{sec:CreateBook})

\item Mit \textit{./create-buildscript.sh} wird das Programmskript
generiert (Kapitel~\ref{sec:CreateBuildscript},
Seite~\pageref{sec:CreateBuildscript}).

\item \textbf{Alternativ:} Herunterladen der Dateien von
\begin{itemize}
    \item \url{https://ddp-team.pages.debian.net/dpb/BuildWithGBP.pdf}
    \item \url{https://ddp-team.pages.debian.net/dpb/create-book.sh}
    \item \url{https://ddp-team.pages.debian.net/dpb/create-buildscript.sh}
\end{itemize}

\item Erstellen von Symlinks auf die generierten Skripte unter
\textit{/usr/local/bin}
Damit liegen die jeweils aktuellen Skripte auch in einem Programmpfad
(\textit{PATH}) und können überall ohne Pfadangabe aufgerufen werden.

\item Installation aller Abhängigkeiten, die das Programmskript benötigt.
(Kapitel~\ref{sec:scriptdependency}, Seite~\pageref{sec:scriptdependency})

\item Anlegen der benötigten Verzeichnisse und Dateien
(Kapitel~\ref{sec:FolderStructure}, Seite~\pageref{sec:FolderStructure})

\begin{itemize}
    \item Verzeichnis für die Konfigurationsdateien
          \textit{\textasciitilde/.debian\_project/}
          (Kapitel~\ref{subsec:ConfigurationFiles},
          Seite~\pageref{subsec:ConfigurationFiles})
          
    \item Anlegen des Projektverzeichnisses und der Unterverzeichnisse
          (Kapitel~\ref{subsec:ProjectPath}, Seite~\pageref{subsec:ProjectPath})
          
    \item Eintragungen in die Datei \textit{\textasciitilde/.bashrc}
          (Kapitel~\ref{subsec:bashrc}, Seite~\pageref{subsec:bashrc})
          
    \item Anlegen der Datei \textit{gbp.conf} (Kapitel~\ref{sec:GbpConf},
          Seite~\pageref{sec:GbpConf})
          
    \item Bei Nutzung des \textit{pbuilder} folgt die Konfiguration
          (Kapitel~\ref{subsec:PbuilderConfiguration},
          Seite~\pageref{subsec:PbuilderConfiguration})
          und Einrichtung der Hooks (Kapitel~\ref{subsec:HooksEinrichten},
          Seite~\pageref{subsec:HooksEinrichten})
\end{itemize}
\end{enumerate}

\newpage

\section{Nutzung des Programmskriptes}

\begin{enumerate}
\item Bereitstellen des GPG-Keys, um Git-Tags zu signieren

\item Ausführen von \textit{build-gbp.sh}
(Kapitel~\ref{sec:Hauptprogramm}, Seite~\pageref{sec:Hauptprogramm})

\item Mit dem Projektnamen die Konfigurationsdatei erstellen
bzw. laden (Kapitel~\ref{sec:ProjektnamenAbfragen},
Seite~\pageref{sec:ProjektnamenAbfragen}).

%\item Bauen eines neuen \texttt{Debian}-Paketes
%(Kapitel~\ref{chap:BuildNewVersion}, Seite~\pageref{chap:BuildNewVersion})
\item Beschaffen des Upstream-Quellcodes (Kapitel~\ref{chap:BuildNewVersion},
Seite~\pageref{chap:BuildNewVersion})

\item Anlegen bzw. Aktualisieren der Dateien im Verzeichnis \textit{debian/}
(Kapitel~\ref{chap:NeueRevisionBauen}, Seite~\pageref{chap:NeueRevisionBauen})

\item Änderungen am Quellcode vornehmen
(Kapitel~\ref{chap:Aenderungen_am_Upstream_Code_vornehmen},
Seite~\pageref{chap:Aenderungen_am_Upstream_Code_vornehmen})

\item Bauen des \texttt{Debian}-Paketes
(Kapitel~\ref{chap:Bauen}, Seite~\pageref{chap:Bauen})

\item Überprüfen der Qualität des gebauten \texttt{Debian}-Paketes
(Kapitel~\ref{chap:Ueberpruefungen}, Seite~\pageref{chap:Ueberpruefungen})

\item Veröffentlichen des \texttt{Debian}-Paketes
(Kapitel~\ref{chap:Uploading}, Seite~\pageref{chap:Uploading})
\end{enumerate}

Das Programmskript ist modular aufgebaut, sodass man an vielen Stellen 
\enquote{aussteigen} und später wieder \enquote{einsteigen} kann. Es ermöglicht auch
manuelle Eingriffe.

Vor dem Arbeiten mit dem Programmskript sollte auch in folgenden Teil
hineingeschaut werden.

